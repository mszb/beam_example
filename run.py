import logging
import apache_beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.io.textio import ReadFromText, WriteToText

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

input_filename = 'stocks.csv'
output_filename = 'output.csv'

options = PipelineOptions()
options.view_as(SetupOptions).save_main_session = True


class Split(apache_beam.DoFn):

    def process(self, element):
        """
        Splits each row on commas and returns a dictionary representing the row
        """
        date, open, high, low, close, volume, name = element.split(",")

        return [{
            'date': date,
            'name': name,
            'open': float(open or 0),
            'high': float(high or 0),
            'low': float(low or 0),
            'volume': float(volume or 0),
            'close': float(close or 0)
        }]


class Printer(apache_beam.DoFn):

    def process(self, data_item):
        print(data_item)


class Daz(apache_beam.DoFn):

    def __init__(self, key):
        self._daz = key

    def process(self, element):
        return [element]


class ToComaSeperated(apache_beam.DoFn):

    def process(self, element):
        return ["{name},{highs},{lows},{vols}".format(
            name=element[0],
            highs=element[1]['highs'][0],
            vols=element[1]['min_vols'][0],
            lows=element[1]['lows'][0]
        )]


with apache_beam.Pipeline(options=options) as p:
    rows = (
            p
            | ReadFromText(input_filename, skip_header_lines=1)
            | apache_beam.ParDo(Split())
    )

    avgs_high = (
            rows
            | apache_beam.Map(lambda x: (x['name'], x['high']))
            | 'Daz high' >> apache_beam.ParDo(Daz("."))
            | 'grouping highs' >> apache_beam.GroupByKey()
            | "Calculating average" >> apache_beam.CombineValues(apache_beam.combiners.MeanCombineFn())
    )

    avgs_lows = (
            rows
            | apache_beam.Map(lambda x: (x['name'], x['low']))
            | 'Daz lows' >> apache_beam.ParDo(Daz("+"))
            | 'Grouping lows' >> apache_beam.GroupByKey()
            | apache_beam.Map(lambda x: (x[0], sum(x[1]) / len(x[1])))
    )

    min_vols = (
            rows
            | apache_beam.Map(lambda x: (x['name'], x['volume']))
            | 'Daz Vols' >> apache_beam.ParDo(Daz("-"))
            | 'Grouping vols' >> apache_beam.GroupByKey()
            | apache_beam.Map(lambda x: (x[0], min(x[1])))
    )

    _ = (
            {
                'highs': avgs_high,
                'min_vols': min_vols,
                'lows': avgs_lows
            }
            | apache_beam.CoGroupByKey()
            # | apache_beam.ParDo(Printer())
            | apache_beam.ParDo(ToComaSeperated())
            | WriteToText(output_filename)
    )

